package com.java.pattern.prototype;

import java.util.HashMap;

public final class ShapeCache {
	
	private ShapeCache(){		
	}

	private static HashMap<String, Shape> shapeMap = new HashMap<String, Shape>();
	
	public static void loadCache(){
		Shape c = new Circle();
		c.setId(Shape.ID_CIRCLE);
		shapeMap.put(Shape.ID_CIRCLE, c);
		
		c = new Square();
		c.setId(Shape.ID_SQUARE);
		shapeMap.put(Shape.ID_SQUARE, c);
		
	}
	
	public static  Shape getShape(String id){
		return shapeMap.get(id);
	}
	
}
