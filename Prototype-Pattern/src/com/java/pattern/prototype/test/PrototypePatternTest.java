package com.java.pattern.prototype.test;

import com.java.pattern.prototype.Shape;
import com.java.pattern.prototype.ShapeCache;




/**
 * 优势：
 * 用于创建重复的对象，同时又能保证性能
 * 
 * 原理：
 * 实现了一个原型接口，该接口用于创建当前对象的克隆
 * 
 * 使用场景：
 * 当直接创建对象的代价比较大时，则采用这种模式。例如，一个对象需要在一个高代价的数据库操作之后被创建。
 * 我们可以缓存该对象，在下一个请求时返回它的克隆，在需要的时候更新数据库，以此来减少数据库调用。
 * 
 */

public class PrototypePatternTest {
	public static void main(String[] args){
		
		ShapeCache.loadCache();
		Shape circle = ShapeCache.getShape(Shape.ID_CIRCLE);
		circle.draw();
		
		Shape square = ShapeCache.getShape(Shape.ID_SQUARE);
		square.draw();	
		
	}
}
