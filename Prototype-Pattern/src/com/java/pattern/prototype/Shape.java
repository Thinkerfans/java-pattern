package com.java.pattern.prototype;

public abstract class Shape implements Cloneable {
	
	public static final String ID_CIRCLE = "circle";
	public static final String ID_SQUARE = "square";

	
	protected String id;
	protected String type;

	public abstract void draw();

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	protected Object clone() {
		Object o = null;
		try {
			o = super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return o;
	}

}
