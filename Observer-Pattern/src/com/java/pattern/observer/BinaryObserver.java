package com.java.pattern.observer;

public class BinaryObserver extends Observer{
	
	
	public BinaryObserver(Subject s) {
		subject = s;
		subject.attachObserver(this);
	}

	@Override
	public void update() {	
		System.out.println("Binary String: "+Integer.toBinaryString(subject.getState()));
	}

}
