package com.java.pattern.observer;

public class HexObserver extends Observer {

	public HexObserver(Subject s) {
		subject = s;
		subject.attachObserver(this);
	}

	@Override
	public void update() {
		System.out.println("Hex String: "+Integer.toHexString(subject.getState()));

	}

}
