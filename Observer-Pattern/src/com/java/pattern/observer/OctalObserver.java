package com.java.pattern.observer;

public class OctalObserver extends Observer {

	public OctalObserver(Subject s) {
		subject = s;
		subject.attachObserver(this);
	}

	@Override
	public void update() {
		System.out.println("Octal String: "+Integer.toOctalString(subject.getState()));

	}

}
