package com.java.pattern.observer;

import java.util.ArrayList;
import java.util.List;

public class Subject {
	
	private int state;

	private List<Observer> mObserver = new ArrayList<Observer>();

	public void attachObserver(Observer o) {
		mObserver.add(o);
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
		notifyObservers();
	}

	public void notifyObservers() {
		for (Observer o : mObserver) {
			o.update();
		}
	}
}
