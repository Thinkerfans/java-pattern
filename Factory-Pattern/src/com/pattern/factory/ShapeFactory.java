package com.pattern.factory;


public class ShapeFactory {
	
	public static final String SHAPE_CIRCLE = "circle";
	public static final String SHAPE_TRIANGLE = "triangle";
	
	private static class ShapeFactoryInnerClass{
		static ShapeFactory singletonInstance = new ShapeFactory();
	}
	
	private ShapeFactory(){
	}
	
	public static ShapeFactory getShapeFactory(){
		return ShapeFactoryInnerClass.singletonInstance;
	}

	public Shape createShape(String shape){
		if(shape == null){
			System.out.println("input shape name cann't be null ");
			return null;
		}
		if(SHAPE_CIRCLE.equals(shape)){
			return new Circle();
		}else if(SHAPE_TRIANGLE.equals(shape)){
			return new Triangle();
		}else{
			System.out.println("not support the shape: "+shape);
			return null;
		}	
	}

}
