package com.pattern.factory.test;

import com.pattern.factory.Shape;
import com.pattern.factory.ShapeFactory;

public class FactoryTest {
	
	public static void main(String[] args){
		
		ShapeFactory factory = ShapeFactory.getShapeFactory();
		Shape circle = factory.createShape(ShapeFactory.SHAPE_CIRCLE);
		circle.draw();
		
		Shape triangle = factory.createShape(ShapeFactory.SHAPE_TRIANGLE);
		triangle.draw();
		
		Shape rectangle = factory.createShape("rectangle");
		rectangle.draw();
	} 
}
