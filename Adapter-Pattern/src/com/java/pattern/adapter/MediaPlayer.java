package com.java.pattern.adapter;

public interface MediaPlayer {
	
	void play(String type,String fileName);

}
