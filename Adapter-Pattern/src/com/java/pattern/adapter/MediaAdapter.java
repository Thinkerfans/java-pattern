package com.java.pattern.adapter;

public class MediaAdapter implements MediaPlayer {
	
	public static final String TYPE_MP3 = "mp3";
	public static final String TYPE_MP4 = "mp4";
	public static final String TYPE_VLC = "vlc";


	private AdvanceMediaPlayer mAdvancePlayer;
	
	public  MediaAdapter(String type) {
		if(TYPE_MP4.equals(type)){
			mAdvancePlayer = new Mp4Player();
		}else if(TYPE_VLC.equals(type)){
			mAdvancePlayer = new VlcPlayer();
		}
	}

	@Override
	public void play(String type, String fileName) {
		if(TYPE_MP4.equals(type)){
			mAdvancePlayer.playMp4(fileName);
		}else if(TYPE_VLC.equals(type)){
			mAdvancePlayer.playVlc(fileName);
		}else{
			System.out.println("Advance mediaPlayer not support ");
		}
	}

}
