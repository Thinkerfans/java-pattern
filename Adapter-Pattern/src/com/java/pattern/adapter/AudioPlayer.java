package com.java.pattern.adapter;

public class AudioPlayer implements MediaPlayer {

	MediaAdapter mAdapter;

	@Override
	public void play(String type, String fileName) {
		if (MediaAdapter.TYPE_MP3.equals(type)) {
			System.out.println("mp3 play " + fileName);
		} else if (MediaAdapter.TYPE_VLC.equals(type)
				|| MediaAdapter.TYPE_MP4.equals(type)) {
			mAdapter = new MediaAdapter(type);
			mAdapter.play(type, fileName);
		} else {
			System.out.println("Audio mediaPlayer not support the format ");
		}
	}

}
