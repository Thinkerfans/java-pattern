package com.java.pattern.facade;

public interface Shape {
	
	void draw();

}
