package com.java.pattern.facade;

public class ShapeMaker {

	private Shape cube;
	private Shape sector;

	public ShapeMaker() {
		cube = new Cube();
		sector = new Sector();
	}

	public void drawCube() {
		cube.draw();
	}

	public void drawSector() {
		sector.draw();
	}
}
