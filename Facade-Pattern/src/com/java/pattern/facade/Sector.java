package com.java.pattern.facade;

public class Sector implements Shape {

	@Override
	public void draw() {
		System.out.println("draw sector");
	}

}
