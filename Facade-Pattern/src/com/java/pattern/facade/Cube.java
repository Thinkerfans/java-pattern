package com.java.pattern.facade;

public class Cube implements Shape {
	@Override
	public void draw() {
		System.out.println("draw cube ");
	}
}
