package com.java.pattern.iterator;

public interface Container {
	Iterator getIterator();
}
