package com.java.pattern.memento;

import java.util.ArrayList;
import java.util.List;

public class CareTaker {
	
	private List<Memento> mList = new ArrayList<Memento>();
	
	public void add(Memento memento){
		mList.add(memento);
	}
	
	public Memento get(int index){
		if(index>=0 && index < mList.size()){
			return mList.get(index);
		}	
		return null;
	}

}
