package com.java.pattern.flyweight;

public interface Shape {
	public void draw();
}