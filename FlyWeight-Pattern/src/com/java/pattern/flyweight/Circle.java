package com.java.pattern.flyweight;

public class Circle implements Shape {
	
	private String color;
	private int x;
	private int y;
	private int radius;
	
	public Circle(String color) {
		this.color = color;
	}

	@Override
	public void draw() {
		System.out.println("draw circle color:"+color+", x:"+ x +", y:"+ y +", radius:"+radius);
	}

	public int getCenterX() {
		return x;
	}

	public void setCenterX(int x) {
		this.x = x;
	}

	public int getCenterY() {
		return y;
	}

	public void setCenterY(int y) {
		this.y = y;
	}

	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}

}
