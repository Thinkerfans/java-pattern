package com.java.pattern.flyweight;

import java.util.HashMap;

public class ShapeFactory {

	private static HashMap<String, Shape> sShapeMap = new HashMap<String, Shape>();

	public static Shape getShape(String color) {
		Shape circle = sShapeMap.get(color);
		if (circle == null) {
			circle = new Circle(color);
			sShapeMap.put(color, circle);
			System.out.println("create circle of color :" + color);
		}
		return circle;
	}
}
