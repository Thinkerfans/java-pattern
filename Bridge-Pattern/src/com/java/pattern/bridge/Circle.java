package com.java.pattern.bridge;

public class Circle  extends Shape{

	public Circle(DrawAPI api) {
		super(api);
	}
	
   public void draw() {
	   mDrawApi.drawCircle();
   }
}
