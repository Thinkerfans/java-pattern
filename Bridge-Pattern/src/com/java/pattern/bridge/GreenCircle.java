package com.java.pattern.bridge;

public class GreenCircle implements DrawAPI {

	@Override
	public void drawCircle() {
		System.out.println("draw green circle");
	}

}
