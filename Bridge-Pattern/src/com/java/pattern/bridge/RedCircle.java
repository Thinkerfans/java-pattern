package com.java.pattern.bridge;

public class RedCircle implements DrawAPI {

	@Override
	public void drawCircle() {
		System.out.println("draw red circle");
	}
}