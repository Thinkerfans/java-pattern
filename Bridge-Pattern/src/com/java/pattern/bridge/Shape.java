package com.java.pattern.bridge;


/**
 * 抽象类通过持有接口类的实例，从而实现两者分离。
 * */
public abstract class Shape {
	
	DrawAPI mDrawApi;
	public Shape(DrawAPI api) {
		this.mDrawApi = api;
	}
	
	public  abstract void draw();	
}
