package com.java.pattern.bridge;

public interface DrawAPI {
	void drawCircle();
}
