package com.java.pattern.strategy;

public class Context {
	private Strategy mStrategy;

	public Context(Strategy strategy) {
		mStrategy = strategy;
	}

	public int executeStrategy(int num1, int num2) {
		return mStrategy.doOperation(num1, num2);
	}

}
