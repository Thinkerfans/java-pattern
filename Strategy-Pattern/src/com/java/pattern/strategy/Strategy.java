package com.java.pattern.strategy;

public interface Strategy {
	int doOperation(int para1,int para2);
}
