package com.java.pattern.strategy;

public class OperationMinus  implements Strategy{

	@Override
	public int doOperation(int para1, int para2) {
		return para1-para2;
	}

}
