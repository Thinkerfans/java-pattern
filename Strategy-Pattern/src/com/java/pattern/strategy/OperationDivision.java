package com.java.pattern.strategy;

public class OperationDivision  implements Strategy{

	@Override
	public int doOperation(int para1, int para2) {
		return para1/para2;
	}

}
