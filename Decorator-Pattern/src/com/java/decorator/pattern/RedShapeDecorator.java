package com.java.decorator.pattern;

public class RedShapeDecorator  extends ShapeDecorator{

	public RedShapeDecorator(Shape shape) {
		super(shape);
	}

	@Override
	public void draw() {
		mDecoratorShape.draw();
		setShapeColor();
	}
	
	private void setShapeColor(){
		System.out.println("red color ");
	}

}
