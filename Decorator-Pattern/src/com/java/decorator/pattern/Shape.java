package com.java.decorator.pattern;

public interface Shape {
	void draw();
}
