package com.java.decorator.pattern;

public abstract class ShapeDecorator implements Shape {
	protected Shape mDecoratorShape;

	public ShapeDecorator(Shape shape) {
		mDecoratorShape = shape;
	}
	
	
}
