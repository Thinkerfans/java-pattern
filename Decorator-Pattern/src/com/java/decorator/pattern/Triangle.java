package com.java.decorator.pattern;

public class Triangle implements Shape {

	@Override
	public void draw() {
		System.out.println("draw triangle");
	}

}
