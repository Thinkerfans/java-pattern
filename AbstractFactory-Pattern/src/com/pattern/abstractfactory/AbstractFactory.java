package com.pattern.abstractfactory;

public abstract class AbstractFactory {
	
	public abstract  Color createColor(String color);
	public abstract  Shape createShape(String shape);
	
}
