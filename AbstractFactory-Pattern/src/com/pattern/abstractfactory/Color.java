package com.pattern.abstractfactory;

public interface Color {
	void fill();
}
