package com.pattern.abstractfactory;

public class FactoryProducer {
	
	public static final String FACTORY_SHAPE = "shape";
	public static final String FACTORY_COLOR = "color";
	
	public  static AbstractFactory getFactory(String factory){
		if(factory == null){
			System.out.println("input factory name cann't be null ");
			return null;
		}
		if(FACTORY_SHAPE.equals(factory)){
			return ShapeFactory.getShapeFactory();
		}else if(FACTORY_COLOR.equals(factory)){
			return ColorFactory.getColorFactory();
		}else{
			System.out.println("not support the factory: "+factory);
			return null;
		}	
	} 
}
