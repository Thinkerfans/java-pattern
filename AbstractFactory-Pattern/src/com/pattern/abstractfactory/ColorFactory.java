package com.pattern.abstractfactory;


public class ColorFactory extends AbstractFactory {
	public static final String COLOR_RED = "red";
	public static final String COLOR_BLUE = "blue";
	
	private ColorFactory(){
	}
	
	private static ColorFactory singletonInstance ;

	public static synchronized ColorFactory getColorFactory(){
		if(singletonInstance == null){
			singletonInstance = new ColorFactory();
		}
		return singletonInstance;
	}

	public Color createColor(String color){
		if(color == null){
			System.out.println("input color name cann't be null ");
			return null;
		}
		if(COLOR_RED.equals(color)){
			return new Red();
		}else if(COLOR_BLUE.equals(color)){
			return new Blue();
		}else{
			System.out.println("not support the color: "+color);
			return null;
		}	
	}

	@Override
	public Shape createShape(String shape) {
		return null;
	}
	
}
