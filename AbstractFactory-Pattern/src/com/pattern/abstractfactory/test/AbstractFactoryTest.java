package com.pattern.abstractfactory.test;

import com.pattern.abstractfactory.AbstractFactory;
import com.pattern.abstractfactory.Color;
import com.pattern.abstractfactory.ColorFactory;
import com.pattern.abstractfactory.FactoryProducer;
import com.pattern.abstractfactory.Shape;
import com.pattern.abstractfactory.ShapeFactory;

public class AbstractFactoryTest {
	
	public static void main(String[] args){
		
		AbstractFactory factory =  FactoryProducer.getFactory(FactoryProducer.FACTORY_SHAPE);
		Shape circle = factory.createShape(ShapeFactory.SHAPE_CIRCLE);
		circle.draw();
		
		Shape triangle = factory.createShape(ShapeFactory.SHAPE_TRIANGLE);
		triangle.draw();
		
		
		factory =  FactoryProducer.getFactory(FactoryProducer.FACTORY_COLOR);
		Color red = factory.createColor(ColorFactory.COLOR_RED);
		red.fill();
		
		Color blue = factory.createColor(ColorFactory.COLOR_BLUE);
		blue.fill();

	} 
}
