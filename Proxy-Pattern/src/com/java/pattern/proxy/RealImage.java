package com.java.pattern.proxy;

public class RealImage implements Image {
	
	private String fileName;

	public RealImage(String name) {
		fileName = name;
		loadFromDisk(name);
	}
	
	@Override
	public void display() {
		System.out.println("display image:"+fileName);
	}
	
	private void loadFromDisk(String fileName){
		System.out.println("load image:"+fileName);
		
	}

}
