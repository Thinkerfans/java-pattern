package com.java.pattern.command;

public class SellStock implements Order {

	private Stock mStock;

	public SellStock(Stock stock) {
		this.mStock = stock;
	}

	@Override
	public void excute() {
		mStock.sell();
	}

}
