package com.java.pattern.command;

public interface Order {

	void excute();

}
