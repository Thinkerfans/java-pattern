package com.java.pattern.command;

public class Stock {
	
	public void buy() {
		System.out.println(" stock buy ");
	}

	public void sell() {
		System.out.println(" stock sell ");
	}
	
}
