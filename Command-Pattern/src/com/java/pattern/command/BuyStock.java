package com.java.pattern.command;

public class BuyStock implements Order {

	private Stock mStock;

	public BuyStock(Stock stock) {
		this.mStock = stock;
	}

	@Override
	public void excute() {
		mStock.buy();
	}

}
