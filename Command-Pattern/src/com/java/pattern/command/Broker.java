package com.java.pattern.command;

import java.util.ArrayList;
import java.util.List;

public class Broker {
	private List<Order> mList = new ArrayList<Order>();

	public void takeOrder(Order order) {
		mList.add(order);
	}

	public void placeOrders() {
		for (Order o : mList) {
			o.excute();
		}
		mList.clear();
	}

}
