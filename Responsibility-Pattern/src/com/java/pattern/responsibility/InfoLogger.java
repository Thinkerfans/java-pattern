package com.java.pattern.responsibility;

public class InfoLogger extends AbstractLogger {

	public InfoLogger(int level) {
		this.level = level;
	}

	@Override
	protected void write(String msg) {
		System.out.println("info log: "+msg);
	}

}
