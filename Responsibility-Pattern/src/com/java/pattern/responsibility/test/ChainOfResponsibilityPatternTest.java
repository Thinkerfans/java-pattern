package com.java.pattern.responsibility.test;

import com.java.pattern.responsibility.AbstractLogger;
import com.java.pattern.responsibility.DebugLogger;
import com.java.pattern.responsibility.ErrorLogger;
import com.java.pattern.responsibility.InfoLogger;

public class ChainOfResponsibilityPatternTest {

	private static AbstractLogger getChainOfLoggers() {

		AbstractLogger infoLogger = new InfoLogger(AbstractLogger.LOG_INFO);
		AbstractLogger debugLogger = new DebugLogger(AbstractLogger.LOG_DEBUG);
		AbstractLogger errorLogger = new ErrorLogger(AbstractLogger.LOG_ERROR);

		errorLogger.setNextLogger(debugLogger);
		debugLogger.setNextLogger(infoLogger);

		return errorLogger;
	}

	public static void main(String[] args) {
		AbstractLogger loggerChain = getChainOfLoggers();
		
		loggerChain.logMessage(AbstractLogger.LOG_INFO, "This is an information.");

		loggerChain.logMessage(AbstractLogger.LOG_DEBUG,
				"This is an debug level information.");

		loggerChain.logMessage(AbstractLogger.LOG_ERROR,
				"This is an error information.");
	}

}
