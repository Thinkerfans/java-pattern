package com.java.pattern.responsibility;

public class DebugLogger extends AbstractLogger {

	public DebugLogger(int level) {
		this.level = level;
	}

	@Override
	protected void write(String msg) {
		System.out.println("debug log: "+msg);
	}

}
