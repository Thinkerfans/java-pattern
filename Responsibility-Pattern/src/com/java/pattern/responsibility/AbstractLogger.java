package com.java.pattern.responsibility;

public abstract class AbstractLogger {
	
	public final static int LOG_INFO = 1;
	public final static int LOG_DEBUG = 2;
	public final static int LOG_ERROR = 3;

	protected int level;

	protected AbstractLogger nextLogger;

	public void setNextLogger(AbstractLogger nextLogger) {
		this.nextLogger = nextLogger;
	}

	public void logMessage(int level, String msg) {
		if (this.level <= level) {
			write(msg);
		}

		if (nextLogger != null) {
			nextLogger.logMessage(level, msg);
		}
	}

	protected abstract void write(String msg);

}
