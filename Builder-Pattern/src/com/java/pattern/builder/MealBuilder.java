package com.java.pattern.builder;

import com.java.meal.burger.ChickenBurger;
import com.java.meal.burger.VegBurger;
import com.java.meal.drink.Coke;
import com.java.meal.drink.Pepsi;

public class MealBuilder {
	
	  public Meal prepareVegMeal (){
	      Meal meal = new Meal();
	      meal.addItem(new VegBurger());
	      meal.addItem(new Coke());
	      return meal;
	   }   

	   public Meal prepareNonVegMeal (){
	      Meal meal = new Meal();
	      meal.addItem(new ChickenBurger());
	      meal.addItem(new Pepsi());
	      return meal;
	   }
}
