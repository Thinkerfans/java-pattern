package com.java.pattern.builder.test;

import com.java.pattern.builder.Meal;
import com.java.pattern.builder.MealBuilder;

public class BuilderPatternTest {

	public static void main(String[] args) {

		MealBuilder builder = new MealBuilder();
		Meal meal = builder.prepareVegMeal();
		meal.showItems();
	    System.out.println("Total Cost: " +meal.getCost());


		meal = builder.prepareNonVegMeal();
		meal.showItems();
	    System.out.println("Total Cost: " +meal.getCost());


	}

}
