package com.java.pattern.builder;

public interface Packing {
	
	public String pack();

}
