package com.java.meal.burger;

import com.java.pattern.builder.Item;
import com.java.pattern.builder.Packing;
import com.java.pattern.builder.Wrapper;

public abstract class Burger implements Item {
	@Override
	public Packing packing() {
		return new Wrapper();
	}

}
