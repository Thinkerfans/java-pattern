package com.java.meal.burger;


public class VegBurger extends Burger{

	@Override
	public String name() {
		return "Vegetable Burger";
	}

	@Override
	public float price() {
		return 30.0f;
	}

}
