package com.java.meal.drink;

import com.java.pattern.builder.Bottle;
import com.java.pattern.builder.Item;
import com.java.pattern.builder.Packing;

public abstract class ColdDrink implements Item {
	
	@Override
	public Packing packing() {
		return new Bottle();
	}

}
