package com.java.pattern.filter;

import java.util.ArrayList;
import java.util.List;

public class SingleFilter implements Filter {

	@Override
	public List<Person> meetCriteria(List<Person> persons) {
		List<Person> singles = new ArrayList<Person>();
		for(Person p:persons){
			if(SINGLE.equals(p.getMaritalStatus())){
				singles.add(p);
			}
		}
		return singles;
	}

}
