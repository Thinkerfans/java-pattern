package com.java.pattern.filter;

import java.util.List;

public interface Filter {
	
		public static final String MALE = "male";
		public static final String FEMALE = "female";
		public static final String SINGLE = "single";
		public static final String MARRIED = "married";
	
	   public List<Person> meetCriteria(List<Person> persons);

}
