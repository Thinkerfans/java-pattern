package com.java.pattern.filter;

import java.util.ArrayList;
import java.util.List;

public class MaleFilter implements Filter {

	@Override
	public List<Person> meetCriteria(List<Person> persons) {
		List<Person> males = new ArrayList<Person>();
		for(Person p:persons){
			if(MALE.equals(p.getGender())){
				males.add(p);
			}
		}
		return males;
	}

}
