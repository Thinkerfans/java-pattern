package com.java.pattern.filter;

import java.util.ArrayList;
import java.util.List;

public class MarriedFilter implements Filter {

	@Override
	public List<Person> meetCriteria(List<Person> persons) {
		List<Person> married = new ArrayList<Person>();
		for(Person p:persons){
			if(MARRIED.equals(p.getMaritalStatus())){
				married.add(p);
			}
		}
		return married;
	}

}
