package com.java.pattern.filter;

import java.util.List;

public class MarriedOrFemaleFilter implements Filter {
	
	MarriedFilter marriedFilter ;
	FemaleFilter femaleFilter;

	public MarriedOrFemaleFilter(MarriedFilter marriedFilter, FemaleFilter femaleFilter) {
		this.marriedFilter = marriedFilter;
		this.femaleFilter = femaleFilter;
	}

	@Override
	public List<Person> meetCriteria(List<Person> persons) {
		List<Person> out = marriedFilter.meetCriteria(persons);
		List<Person> female = femaleFilter.meetCriteria(persons);
		for(Person p:female){
			if(!out.contains(p)){
				out.add(p);
			}
		}
		return out;
	}
}
