package com.java.pattern.filter;

import java.util.List;

public class SingleMaleFilter implements Filter {
	
	SingleFilter singleFilter ;
	MaleFilter maleFilter;

	public SingleMaleFilter(SingleFilter singleFilter, MaleFilter maleFilter) {
		this.singleFilter = singleFilter;
		this.maleFilter = maleFilter;
	}

	@Override
	public List<Person> meetCriteria(List<Person> persons) {
		List<Person> males = maleFilter.meetCriteria(persons);
		return singleFilter.meetCriteria(males);
	}
}
