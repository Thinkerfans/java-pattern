package com.java.pattern.filter;

import java.util.ArrayList;
import java.util.List;

public class FemaleFilter implements Filter {

	@Override
	public List<Person> meetCriteria(List<Person> persons) {
		List<Person> female = new ArrayList<Person>();
		for(Person p:persons){
			if(FEMALE.equals(p.getGender())){
				female.add(p);
			}
		}
		return female;
	}

}
