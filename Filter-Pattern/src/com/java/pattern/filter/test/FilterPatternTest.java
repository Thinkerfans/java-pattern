package com.java.pattern.filter.test;

import java.util.ArrayList;
import java.util.List;

import com.java.pattern.filter.FemaleFilter;
import com.java.pattern.filter.Filter;
import com.java.pattern.filter.MaleFilter;
import com.java.pattern.filter.MarriedFilter;
import com.java.pattern.filter.MarriedOrFemaleFilter;
import com.java.pattern.filter.Person;
import com.java.pattern.filter.SingleFilter;
import com.java.pattern.filter.SingleMaleFilter;

/**
 * 过滤器模式（Filter Pattern）或标准模式（Criteria
 * Pattern）是一种设计模式，这种模式允许开发人员使用不同的标准来过滤一组对象，通过逻辑运算以解耦的方式把它们连接起来
 * 这种类型的设计模式属于结构型模式，它结合多个标准来获得单一标准。
 * */
public class FilterPatternTest {

	public static void main(String[] args) {

		List<Person> persons = new ArrayList<Person>();

		persons.add(new Person("Robert", Filter.MALE, Filter.SINGLE));
		persons.add(new Person("John", Filter.MALE, Filter.MARRIED));
		persons.add(new Person("Laura", Filter.FEMALE, Filter.SINGLE));
		persons.add(new Person("Diana", Filter.FEMALE, Filter.MARRIED));
		persons.add(new Person("Mike", Filter.MALE, Filter.SINGLE));
		persons.add(new Person("Lucy", Filter.FEMALE, Filter.SINGLE));

		MaleFilter male = new MaleFilter();
		FemaleFilter female = new FemaleFilter();
		SingleFilter single = new SingleFilter();
		MarriedFilter married = new MarriedFilter();

		SingleMaleFilter singleMale = new SingleMaleFilter(single, male);
		MarriedOrFemaleFilter marriedOrFemale = new MarriedOrFemaleFilter(
				married, female);

		System.out.println("Males: ");
		printPersons(male.meetCriteria(persons));

		System.out.println("\nFemales: ");
		printPersons(female.meetCriteria(persons));

		System.out.println("\nSingle and Males: ");
		printPersons(singleMale.meetCriteria(persons));

		System.out.println("\n married Or Females: ");
		printPersons(marriedOrFemale.meetCriteria(persons));

	}

	public static void printPersons(List<Person> persons) {
		for (Person person : persons) {
			System.out.println("Person : [ Name : " + person.getName()
					+ ", Gender : " + person.getGender()
					+ ", Marital Status : " + person.getMaritalStatus() + " ]");
		}
	}

}
