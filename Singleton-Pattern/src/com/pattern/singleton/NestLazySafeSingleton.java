package com.pattern.singleton;

public class NestLazySafeSingleton {
	
	private static class NestLazySafeSingletonHolder{
		private final static NestLazySafeSingleton sInstance = new NestLazySafeSingleton();	
	}
	
	private NestLazySafeSingleton(){		
	}
	
	public static NestLazySafeSingleton getInstance(){
		return NestLazySafeSingletonHolder.sInstance;
	}

}
