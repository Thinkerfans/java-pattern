package com.pattern.singleton;


/*
 * double-checked locking
 * */
public class EfficientLazySafeSingleton {
	
	private static volatile EfficientLazySafeSingleton sInstance;
	
	private EfficientLazySafeSingleton(){		
	}
	
	public static EfficientLazySafeSingleton getInstance(){
		if(sInstance == null){
			synchronized (EfficientLazySafeSingleton.class) {
				if(sInstance == null){
					sInstance = new EfficientLazySafeSingleton();
				}
			}
		}
		return sInstance;
	}
}
