package com.pattern.singleton;

public class LazySafeSingleton {
	
	private static LazySafeSingleton sInstance;
	
	private LazySafeSingleton(){		
	}
	
	public static synchronized LazySafeSingleton getInstance(){
		if(sInstance == null){
			sInstance = new LazySafeSingleton();
		}
		return sInstance;
	}
}
