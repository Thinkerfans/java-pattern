package com.pattern.singleton;

public class EagerSafeSingleton {
	
	
	private final static EagerSafeSingleton sInstance = new EagerSafeSingleton();
	
	private EagerSafeSingleton(){		
	}
	
	public static EagerSafeSingleton getInstance(){
		return sInstance;
	}

}
