package com.pattern.singleton;

public class LazyUnsafeSingleton {
	
	private static LazyUnsafeSingleton sInstance;
	
	private LazyUnsafeSingleton(){		
	}
	
	public static LazyUnsafeSingleton getInstance(){
		if(sInstance == null){
			sInstance = new LazyUnsafeSingleton();
		}
		return sInstance;
	}
}
