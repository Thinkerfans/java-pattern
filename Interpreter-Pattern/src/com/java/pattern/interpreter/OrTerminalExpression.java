package com.java.pattern.interpreter;

public class OrTerminalExpression implements Expression {

	private Expression exp1;
	private Expression exp2;

	public OrTerminalExpression(Expression exp1, Expression exp2) {
		this.exp1 = exp1;
		this.exp2 = exp2;
	}

	@Override
	public boolean interpret(String content) {
		return exp1.interpret(content) || exp2.interpret(content);
	}

}
