package com.java.pattern.interpreter;

public interface Expression {
	public boolean interpret(String content);
}
