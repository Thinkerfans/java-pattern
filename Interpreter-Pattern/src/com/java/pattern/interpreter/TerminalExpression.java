package com.java.pattern.interpreter;

public class TerminalExpression implements Expression {

	private String data;

	public TerminalExpression(String content) {
		this.data = content;
	}

	@Override
	public boolean interpret(String content) {
		if (content.contains(data)) {
			return true;
		}
		return false;
	}

}
