package com.java.pattern.template;

public abstract class GameTemplate {
	abstract void initGame();
	abstract void startGame();
	abstract void stopGame();
	
	/** 模版方法*/
	public final void play(){
		initGame();
		startGame();
		stopGame();
	} 
	
}
