package com.java.pattern.template;

public class Cricket extends GameTemplate {

	@Override
	void initGame() {
		System.out.println(" init cricket game ");
	}

	@Override
	void startGame() {
		System.out.println(" start cricket game ");
		
	}

	@Override
	void stopGame() {
		System.out.println(" stop cricket game ");
		
	}

}
