package com.java.pattern.template;

public class Football extends GameTemplate {

	@Override
	void initGame() {
		System.out.println(" init football game ");
	}

	@Override
	void startGame() {
		System.out.println(" start football game ");
		
	}

	@Override
	void stopGame() {
		System.out.println(" stop football game ");
		
	}
}